/*
  * g++ Cuadrado.cpp -o Cuadrado
*/

#include <iostream>
#include "Pila.h"
#include "Contenedor.h"
using namespace std;

// se agregan containers
void agregar_puerto(Pila **puerto, int max, int area_puerto){
  int valor;

  for(int i = 0; i < area_puerto; i++){
    puerto[i] = new Pila(max);
    for(int j = 0; j < max; j++){
      puerto[i]->push();
    }
  }
}

// menu
void menu() {
   cout << "***BIENVENIDOS AL PUERTO COSTERO***" << endl;
   cout << "INSTRUCCIONES PARA INTERACTUAR EN EL SITIO:" << endl;
   cout << " PRESIONE: 1 PARA AGREGAR/PUSH CONTAINER" << endl;
   cout << " PRESIONE: 2 PARA REMOVER/POP CONTAINER" << endl;
   cout << " PRESIONE: 3 PARA VER PUERTO" << endl;
   cout << " PRESIONE: 0 PARA SALIR" << endl;
}

// main
int main(int argc, char const *argv[]) {
  int opcion;
  int area_puerto;
  int dato;
  int max = 0;
  Pila *puerto[area_puerto];

  // previa interacción para definir tamaño de pilas y puerto
  cout << "PREVIO AL INICIO DEL PROGRA SE SOLICITAN LOS SIGUIENTES DATOS:" << endl;
  cout << "INGRESE TAMAÑO DEL PUERTO:" << endl;
  cin >> area_puerto;
  cout << "INGRESE TAMAÑO DE LA PILA:" << endl;
  cin >> max;

  // desarrollo main
  do {
    menu();
    cin >> opcion;

    switch (opcion) {
      case 1:
        cout << "**AGREGAR**" << endl;
        agregar_puerto(puerto, max, area_puerto);
      break;

      case 2:
        //puerto.pop(valor);
      break;

      case 3:
        //puerto.mostrar_pila();
      break;
  }
} while(opcion != 0);
   return 0;
 }
