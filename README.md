# Guia 2 Pilas(con arreglos)
En un puerto seco se guarda mercaderı́a en contenedores. No es posible colocar más de n contenedores
uno encima del otro y no hay área para más de m pilas de contenedores. Cada contenedor tiene un número
y un nombre de la empresa propietaria. n y m pueden ser distintos. se solicita escribir un programa que 
permita gestionar el ingreso y salida de contenedores, para retirar un contenedor es necesario retirar
los contenedores que están encima de él y colocarlos en otra pila.

# Como funciona
El programa funciona mediante un menu principal, el cual muestra las opciones correspondientes a realizar
en el programa siendo 1 para agregar contenedor, 2 para eliminar el contenedor, 3 para mostrar la pila y 0 para salir.
Mediante el uso de funciones como lo son push(), pop() y mostrar_pila(), desarrollaremos la interacción con el puerto 
(el objeto pila), añadiendo, eliminando y mostrando los cambios generados en el "puerto" en cuanto a los contenedores.
*Detalle: Problemas el día 20, en donde el programa me arroja error ¨Violación de segmento (`core' generado)¨.


# Para ejecutar
Se debe abrir la terminal e ingresar al directorio donde se guardó el ejercicio con sus archivos .cpp y .h
repectivamente. Luego ejecutar el comando make para su compilación (se recomienda hacer un make clean para
iniciar el programa sin ningún contratiempo) y luego ejecutar el comando ./puerto para iniciar el programa.

# Construido con 

- Ubuntu: sistema operativo

- C++: lenguaje de programación

- iostream: libreria

- Atom: editor de texto

# Versiones

- Ubuntu 20.04 LTS

- Atom 1.46.0

# Autor

- Martín Muñoz Vera 
