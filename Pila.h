#include <iostream>
#include "Contenedor.h"

using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila {
  private:
    int tope = 0;
    int max;
    bool band;
    Contenedor *pila_arreglo = NULL;

  public:
    // contructor
    Pila(int max);
    // funciones pila puerto
    void pila_vacia();
    void pila_llena();
    void push();
    void pop(int dato);
    void mostrar_pila();
};
#endif
