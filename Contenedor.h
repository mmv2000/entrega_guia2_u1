#include <iostream>
using namespace std;

#ifndef CONTENEDOR_H
#define CONTENEDOR_H

class Contenedor{
  private:
    int num;
    string empresa;

  public:
    // contructor
    Contenedor();
    // metodos set y get
    string get_num();
    string get_empresa();
    void set_num(int num);
    void set_empresa(string empresa);
    void datos_contenedor();
};
#endif
