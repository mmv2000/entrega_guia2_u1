/*
  * g++ Cuadrado.cpp -o Cuadrado
 */

#include <iostream>
#include "Pila.h"
#include "Contenedor.h"
using namespace std;

// constructor
Pila::Pila(int max) {
  this-> max = max;
  this-> pila_arreglo = new Contenedor[this-> max];
}

// funciones para desarrollo programa
void Pila::pila_vacia() {
  if(this->tope == 0) {
    // Pila vacia
    this->band = true;
  }
  else {
    // Pila llena
    this->band = false;
  }
}

void Pila::pila_llena() {
  if(this->tope == this->max){
    // Pila vacia
    this->band = true;
  }
  else{
    // Pila llena
    this->band = false;
  }
}

void Pila::push(){
  pila_llena();

  if(this->band == true){
    cout << "Desbordamiento, pila llena" << endl;
  }
  else {
    this->tope = this->tope + 1;
    this->pila_arreglo[this->tope - 1].get_num();
    this->pila_arreglo[this->tope - 1].get_empresa();
  }
}

void Pila::pop(int valor){
  pila_vacia();

  if(this->band == true){
    cout << "Subdesbordamiento, pila vacia" << endl;
  }
  else{
    valor = this->pila_arreglo[this->tope];
    this->tope = this->tope - 1;
  }
}

void Pila::mostrar_pila(){
  cout << "**PILA**" << endl;

  for(int i = this->tope; i>= 1; i--){
    cout << pila_arreglo[i].get_num() << endl;
    cout << pila_arreglo[i].get_empresa() << endl;
  }
}
