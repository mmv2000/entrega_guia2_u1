prefix=/usr/local
CC = g++

CFLAGS = -g -Wall
SRC = puerto.cpp Pila.cpp Contenedor.cpp
OBJ = puerto.o Pila.o Contenedor.o
APP = puerto

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
